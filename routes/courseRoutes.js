const express = require("express"); //accessing package of express
const router = express.Router(); // use dot notation to access content of a package

const courseController = require("../controllers/courseController.js");

const auth = require ("../auth.js");

// Create a single course

router.post("/create", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/all", (req,res) => {
	courseController.getAllcourse().then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req,res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})


router.get("/:courseId", (req,res) => {
							//retrieves the id from the url
	courseController.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController))
})

//lclhst4000/crs001/update - valid
//lclhst4000/update/crs001 -  error

router.patch("/:courseId/update", auth.verify, (req,res) => {
	const newData = {
		course: req.body,	//req.headers.authorizatoin contains jwt
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.updateCourse(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = router;

