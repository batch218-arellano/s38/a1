const User = require("../models/user.js");
const bcrypt = require ("bcrypt");
const auth = require("../auth.js");


module.exports.checkEmailExist = (reqBody) => {

	// ".find" - a mongoose crud operation (query) to find a field value from a collection.
	return User.find({email:reqBody.email}).then(result =>{
		// condition if there is an existing user.
		if(result.length > 0){
			return true;
		}
		//condition if there is no exisiting user.
		else {
			return false;
		}
	})
}


	module.exports.registerUser = (reqBody) => {

		let newUser = new User({
			firstName : reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
		/*
			// bcyrpt - package for password hashing
			// hashSync - synchronously generate a hash
			// hash - asynchronously generate a hash.
			// hashing - converts a value to another value.
		*/ 
			password: bcrypt.hashSync(reqBody.password, 10),
		/*
			// 10 = salt rounds
			// Sal rounds is proportional to hasing rounds, the higher the salt rounds, the more hashing rounds, the longer it takes to generate an output
		*/
			mobileNo: reqBody.mobileNo

		})


		return newUser.save().then((user,error) =>{
			if (error){
				return false;
			}
			else{
				return true;
			}
		})

	}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			// compareSync is bcrypt function to compare an unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password does not match.
				return false;
				// return "Incorrect password"
			}
		}
	})
}

/// activity 

/* 
module.exports.detailsUser = (reqBody) => {
	let newUsers = new User({
		password : 'reqBody.password'
	})

	 return User.findOne({id: reqBody.id}).then(result =>{
	 	if(result == null){
			return false;
		}
	 	else {
	 		return result; 
	 		}
		})
	 	
	 }

*/ 


module.exports.detailsUser = (reqBody) => {

	 return User.findOne({_id: reqBody.id}).then(result =>{
	 	if(result == null){
			return false;
		}
	 	else {
	 		result.password = '****';
	 		return result; 
	 		}
		})
	 	
	 }

