const mongoose = require("mongoose");

const Course = require ("../models/course.js");

module.exports.addCourse = (reqBody) =>{

	let newCourse = new Course ({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	})

	return newCourse.save().then((newCourse, error) =>{

		if(error){
			return error; 
		}
		else {
			return newCourse; 
		}
	})
}

module.exports.getAllcourse = () => {
	return Course.find({}).then(result => {
		return result
	})
}

// Get all Active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
		return result
	})
}

// GET specific course
module.exports.getCourse = (courseId) => {
	                    //inside the parenthesis should be the id
	return Course.findById(courseId).then(result => {
		return result;
	})
}

// 1 function 1 return
// 1 function 1 return (function - 1 return)

							// it will contain multiple fields
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true) {

		// update code
		return Course.findByIdAndUpdate(courseId, 
		{    // req.body
			name: newData.course.name,
			description: newData.course.description,
			price: newData.course.price
		}
	  ).then((updatedCourse, error) => {
	  	 if(error){
	  	 	return false
	  	 }
	  	 return true
	  })

	}
		else{
	  		let message = Promise.resolve('User must be ADMIN to access this')
	  		return message.then((value) => {return value})
	  	}

}



